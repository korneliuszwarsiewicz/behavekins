Feature: Test integral calculation function
  Scenario: Integral calculation
    Given Calculator app is run
    When I input "|05x" to calculator
    Then I get result "12.5"

  Scenario: Integral calculation
    Given Calculator app is run
    When I input "|13x+2*x" to calculator
    Then I get result "12.0"

  Scenario: Integral calculation
    Given Calculator app is run
    When I input "|12x*x" to calculator
    Then I get result "2.33"