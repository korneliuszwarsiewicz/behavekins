Feature: Test Calculation Of Mature Exam Exercise
  Scenario: Calculate field of rhombus from mature exam
    Given Calculator app is run
    When I input "rs4a150" to calculator
    Then I get result "8"

  Scenario: Calculate field of rhombus
    Given Calculator app is run
    When I input "rs6a160" to calculator
    Then I get result "10.8"

  Scenario: User doesn't input correct value
    Given Calculator app is run
    When I input "rc4a150" to calculator
    Then I get result "-1"

  Scenario: Calculate cuboid diagonal from mature exam
    Given Calculator app is run
    When I input "d235" to calculator
    Then I get result "6.16"

  Scenario: Calculate cuboid diagonal
    Given Calculator app is run
    When I input "d518" to calculator
    Then I get result "9.49"

  Scenario: User doesn't input correct value of sides
    Given Calculator app is run
    When I input "d890" to calculator
    Then I get result "-1"